class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    @name = "#{@first_name} #{@last_name}"
  end

  def enroll(course)
    return raise 'error' if @courses.any? {|enrolled_course| course.conflicts_with?(enrolled_course) }

    if !@courses.include?(course)
      @courses << course
      course.students << self
    end
  end

  def course_load
    departments = Hash.new(0)

    @courses.map do |course|
      departments[course.department] += course.credits
    end

    departments
  end

end
